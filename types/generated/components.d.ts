import type { Schema, Attribute } from '@strapi/strapi';

export interface ColorColor extends Schema.Component {
  collectionName: 'components_color_colors';
  info: {
    displayName: 'color';
    icon: 'brush';
    description: '';
  };
  attributes: {
    color: Attribute.String &
      Attribute.CustomField<'plugin::color-picker.color'>;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'color.color': ColorColor;
    }
  }
}
