'use strict';

/**
 * slider-inicio router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::slider-inicio.slider-inicio');
