'use strict';

/**
 * slider-inicio controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::slider-inicio.slider-inicio');
